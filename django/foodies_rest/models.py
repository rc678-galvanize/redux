from django.db import models


class MenuItem(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    name = models.TextField()
    calories = models.IntegerField()

    class Meta:
        db_table = "menu_items"


class Review(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    title = models.TextField()
    content = models.TextField()
    reviewer = models.ForeignKey(
        "TruckUser",
        models.DO_NOTHING,
        blank=True,
        null=True,
    )
    rating = models.IntegerField()
    truck = models.ForeignKey("Truck", models.DO_NOTHING)

    class Meta:
        db_table = "reviews"


class TruckMenuItem(models.Model):
    truck = models.ForeignKey("Truck", models.DO_NOTHING)
    menu_item = models.ForeignKey(MenuItem, models.DO_NOTHING)
    price = models.IntegerField()

    class Meta:
        db_table = "truck_menu_items"


class Truck(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    name = models.TextField()
    website = models.TextField()
    category = models.TextField()
    vegetarian_friendly = models.BooleanField()
    owner = models.ForeignKey("TruckUser", models.DO_NOTHING)

    class Meta:
        db_table = "trucks"
        ordering = ["name"]


class TruckUser(models.Model):
    id = models.AutoField(unique=True, primary_key=True)
    first = models.TextField()
    last = models.TextField()
    avatar = models.TextField()
    email = models.TextField(unique=True)
    username = models.TextField(unique=True)
    referrer = models.ForeignKey(
        "self",
        models.DO_NOTHING,
        blank=True,
        null=True,
    )

    class Meta:
        db_table = "users"
