from fastapi import APIRouter, Depends
from pydantic import BaseModel

from db import MenuItemQueries

router = APIRouter()


class MenuItemOut(BaseModel):
    id: int
    name: str
    calories: int


class MenuItemsOut(BaseModel):
    menu_items: list[MenuItemOut]


@router.get("/api/trucks/{truck_id}/menu_items", response_model=MenuItemsOut)
def get_menu_items(
    truck_id: int,
    queries: MenuItemQueries = Depends(),
):
    records = queries.get_menu_items_for_truck(truck_id)
    return {"menu_items": records}
