from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
import os

from routers import menu_items, users, trucks


app = FastAPI()


origins = [
    "http://localhost:3000",
    os.environ.get("CORS_HOST", "http://localhost:3001"),
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.include_router(users.router)
app.include_router(trucks.router)
app.include_router(menu_items.router)
