import { useState } from 'react';
import { useNavigate } from 'react-router-dom';
import ErrorNotification from './ErrorNotification';
import { useCreateOwnerMutation } from './store/ownersApi';
import BulmaInput from './BulmaInput';


function OwnerForm() {
  const navigate = useNavigate();
  const [first, setFirst] = useState('');
  const [last, setLast] = useState('');
  const [avatar, setAvatar] = useState('');
  const [email, setEmail] = useState('');
  const [username, setUsername] = useState('');
  const [error, setError] = useState('');
  const [createOwner, result] = useCreateOwnerMutation();

  async function handleSubmit(e) {
    e.preventDefault();
    createOwner({first, last, avatar, email, username});
  }

  if (result.isSuccess) {
    navigate("/owners");
  } else if (result.isError) {
    setError(result.error);
  }

  return (
    <div className="container">
      <div className="columns is-centered">
        <div className="column is-one-third">
          <ErrorNotification error={error} />
          <form onSubmit={handleSubmit}>
          <BulmaInput
              label="First name"
              id="first"
              placeholder="Noor"
              value={first}
              onChange={setFirst} />
            <BulmaInput
              label="Last name"
              id="last"
              placeholder="Sayid"
              value={last}
              onChange={setLast} />
            <BulmaInput
              label="Email"
              id="email"
              type="email"
              placeholder="noor@sayid.com"
              value={email}
              onChange={setEmail} />
            <BulmaInput
              label="User name"
              id="username"
              placeholder="noor.sayid"
              value={username}
              onChange={setUsername} />
            <BulmaInput
              label="Avatar"
              id="avatar"
              type="url"
              placeholder="https://my.avatar.com/avatar"
              value={avatar}
              required={false}
              onChange={setAvatar} />
            <div className="field">
              <button className="button is-primary">Save</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default OwnerForm;
