import { useEffect, useState } from 'react';
import { useGetTrucksQuery } from './store/trucksApi';
import ErrorNotification from './ErrorNotification';

function TruckList() {
  const { data, error, isLoading } = useGetTrucksQuery();

  if (isLoading) {
    return (
      <progress className="progress is-primary" max="100"></progress>
    );
  }

  return (
    <div className="columns is-centered">
      <div className="column is-narrow">
        <ErrorNotification error={error} />
        <table className="table is-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Veg friendly?</th>
              <th>Category</th>
              <th>Owner</th>
            </tr>
          </thead>
          <tbody>
            {data.trucks.map(truck => (
              <tr key={truck.id}>
                <td>
                  <a href={truck.website}>{truck.name}</a>
                </td>
                <td className="has-text-centered">
                  {truck.vegetarian_friendly ? 'Yes' : ''}
                </td>
                <td>
                  {truck.category}
                </td>
                <td>
                  {truck.owner.first}
                  {truck.owner.last}
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default TruckList;
