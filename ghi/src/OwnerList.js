import { Link } from 'react-router-dom';
import { useGetOwnersQuery } from './store/ownersApi';
import { useDeleteOwnerMutation } from './store/ownersApi';
import ErrorNotification from './ErrorNotification';
import { useSelector, useDispatch } from 'react-redux'
import { increment, selectCount } from './store/store'


function OwnerList() {
  const count = useSelector(selectCount);
  const dispatch = useDispatch();

  const { data, error, isLoading } = useGetOwnersQuery();

	// "Mutations are used to send data updates to the server"
  const [deleteOwner] = useDeleteOwnerMutation();

  if (isLoading) {
    return (
      <progress className="progress is-primary" max="100"></progress>
    );
  }

  return (
    <div className="columns is-centered">
      <div className="column is-narrow">
        <ErrorNotification error={error} />
        {/* Dispatch an action to a reducer */}
        <button onClick={() => dispatch(increment())}> Counter: {count} </button>
        <div className="field has-text-right">
          <Link to="/owners/new" className="button">Add owner</Link>
        </div>
        <table className="table is-striped">
          <thead>
            <tr>
              <th>First</th>
              <th>Last</th>
              <th>Email</th>
              <th>User name</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data.users.map(owner => (
              <tr key={owner.id}>
                <td>{owner.first}</td>
                <td>{owner.last}</td>
                <td>{owner.email}</td>
                <td>{owner.username}</td>
                {/* Reference the mutation function */}
                <td><button onClick={ () => { deleteOwner(owner.id); }}>Delete</button></td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  );
}

export default OwnerList;