import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

// Similar to createSlice, where we take in path and then map to reducer
// createApi documentation: https://redux-toolkit.js.org/rtk-query/api/createApi#query
export const ownersApi = createApi({
  reducerPath: 'owners',
  baseQuery: fetchBaseQuery({
    // http://localhost: 8000
    baseUrl: process.env.REACT_APP_FAST_API,
  }),
  // builder documentation: https://redux-toolkit.js.org/rtk-query/overview
  endpoints: builder => ({
    getOwners: builder.query({
      query: () => '/api/users',
      providesTags: ['OwnerList'],
    }),
    createOwner: builder.mutation({
      query: data => ({
        url: '/api/users',
        body: data,
        method: 'post',
      }),
      invalidatesTags: ['OwnerList'],
    }),
		// Here is a mutation. It is like a reducer, but it is tied to a backend endpoint.
    deleteOwner: builder.mutation({
      query: user_id => ({
        url: '/api/users/' + user_id,
        method: 'delete',
      }),
      // cache invalidation (forces a rerender, based on data)
      invalidatesTags: ['OwnerList'],
    }),
  }),
});

// Exported the useDeleteOwnerMutation.
// Documentation: https://redux-toolkit.js.org/rtk-query/usage/mutations
export const {
  useCreateOwnerMutation,
  useDeleteOwnerMutation,
  useGetOwnersQuery
} = ownersApi;