import { configureStore } from '@reduxjs/toolkit';
import { setupListeners } from '@reduxjs/toolkit/query'
import { ownersApi } from './ownersApi';
import { trucksApi } from './trucksApi';
import { createSlice } from '@reduxjs/toolkit'

export const counterSlice = createSlice({
  name: 'counterSlice',
  initialState: {
    value: 0,
  },
  reducers: {
    increment: (state) => {
      // Redux Toolkit allows us to write "mutating" logic in reducers. It
      // doesn't actually mutate the state because it uses the immer library,
      // which detects changes to a "draft state" and produces a brand new
      state.value += 1
    },
  },
})

// .reducer is auto-generated.
// See official docs here: https://redux-toolkit.js.org/rtk-query/overview
export const store = configureStore({
  reducer: {
    [trucksApi.reducerPath]: trucksApi.reducer,
    [ownersApi.reducerPath]: ownersApi.reducer,
    "count": counterSlice.reducer,
  },
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware()
      .concat(trucksApi.middleware)
      .concat(ownersApi.middleware),
});



// action definition
export const { increment } = counterSlice.actions

// selector
export const selectCount = (state) => state.count.value
export default counterSlice.reducer

setupListeners(store.dispatch);
