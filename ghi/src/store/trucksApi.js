import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react'

export const trucksApi = createApi({
  reducerPath: 'trucks',
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_DJANGO_API,
  }),
  endpoints: builder => ({
    getTrucks: builder.query({
      query: () => '/api/trucks/',
    }),
  }),
});

export const { useGetTrucksQuery } = trucksApi;
